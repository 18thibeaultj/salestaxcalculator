/**
 * Calculates sales tax based on user-input values for tax rate and price.
 * 
 * @author Josh Thibeault
 * @block Blue 3, Hunt
 * @course AP Computer Science
 * 
 * Date created: 09/15/2017
 */

import java.util.Scanner;

public class SalesTaxCalculator {
	
	private static Scanner keyReader = new Scanner(System.in);; // Defines a Scanner object - static to protect method
	
	public static void main(String[] args) {
		double taxRate = getTaxRate(); // Call getTaxRate()
		double basePrice;
		double totalPrice;
		
		while(true) {
			System.out.print("Enter base price (negative value to quit): $");
			basePrice = keyReader.nextDouble();
			
			if(basePrice < 0) {
				System.exit(0);
			} else {
				totalPrice = basePrice + taxRate * basePrice;
				System.out.println("Final price: $" + (totalPrice));
				//@TODO Round double to two decimal places
			}
		}
	}
	
	private static double getTaxRate() {
		System.out.print("Enter the sales tax rate for your area (in %, omit sign): ");
		return (keyReader.nextDouble() / 100); // Convert input from percent form to decimal form, return the value
	}

}
